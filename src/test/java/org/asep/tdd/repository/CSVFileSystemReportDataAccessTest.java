package org.asep.tdd.repository;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.asep.tdd.TestUtils;
import org.asep.tdd.data.Report;
import org.asep.tdd.repository.exception.ReportReadingException;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;

public class CSVFileSystemReportDataAccessTest {

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private CSVFileSystemReportDataAccess reportDataAccess;
	private Path baseDir;

	@Before
	public void setUp() throws Exception {
		baseDir = Paths.get(temporaryFolder.getRoot().getPath());

		reportDataAccess = new CSVFileSystemReportDataAccess(baseDir);
	}

	@Test
	public void constructorThrowsIllegalArgumentExceptionWhenNullBaseDirPassed() throws Exception {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("baseDir cannot be null");

		new CSVFileSystemReportDataAccess(null);
	}

	@Test
	public void constructorThrowsIllegalArgumentExceptionWhenNonExistantBaseDirPassed() throws Exception {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("baseDir must exist and be a directory");

		new CSVFileSystemReportDataAccess(baseDir.resolve("doesnt_exist"));
	}

	@Test
	public void constructorThrowsIllegalArgumentExceptionWhenFileBaseDirPassed() throws Exception {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("baseDir must exist and be a directory");

		File file = temporaryFolder.newFile();

		new CSVFileSystemReportDataAccess(file.toPath());
	}

	@Test
	public void getAllReports() throws Exception {
		TestUtils.copyFileFromClasspath("csv-files/ReportOne.csv", baseDir.resolve("reportOne.csv"));
		TestUtils.copyFileFromClasspath("csv-files/ReportTwo.csv", baseDir.resolve("reportTwo.csv"));

		List<Report> allReports = reportDataAccess.getAllReports();

		Matcher[] reportMatchers = {
				TestUtils.matchesReport(TestUtils.createReport(546, "Tyre", 5)),
				TestUtils.matchesReport(TestUtils.createReport(663, "Spanner", 6)),
				TestUtils.matchesReport(TestUtils.createReport(223, "Coconut", 42)),
				TestUtils.matchesReport(TestUtils.createReport(224, "Banana", 42))
		};

		assertThat(allReports, containsInAnyOrder(reportMatchers));
	}

	@Test
	public void getAllReportsWhenFileContainsBadFormedData() throws Exception {
		expectedException.expect(ReportReadingException.class);

		TestUtils.copyFileFromClasspath("csv-files/ReportBadForm.csv", baseDir.resolve("ReportBadForm.csv"));

		reportDataAccess.getAllReports();
	}

}