package org.asep.tdd;

import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import org.apache.commons.io.IOUtils;
import org.asep.tdd.data.Report;
import org.asep.tdd.repository.CSVFileSystemReportDataAccessTest;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeDiagnosingMatcher;

public abstract class TestUtils {

	private TestUtils() {

	}

	public static Matcher<Report> matchesReport(Report expected) {
		return new TypeSafeDiagnosingMatcher<Report>() {
			@Override
			protected boolean matchesSafely(Report actual, Description mismatchDescription) {
				boolean matches = true;

				Matcher<Integer> itemIdMatcher = Matchers.equalTo(expected.getItemId());
				if (!itemIdMatcher.matches(actual.getItemId())) {
					matches = false;

					itemIdMatcher.describeMismatch(actual.getItemId(), mismatchDescription.appendText("itemId"));
				}

				Matcher<String> itemNameMatcher = Matchers.equalTo(expected.getItemName());
				if (!itemNameMatcher.matches(actual.getItemName())) {
					matches = false;

					itemNameMatcher.describeMismatch(actual.getItemName(), mismatchDescription.appendText("itemName"));
				}

				Matcher<Integer> numberSoldMatcher = Matchers.equalTo(expected.getNumberSold());
				if (!numberSoldMatcher.matches(actual.getNumberSold())) {
					matches = false;

					numberSoldMatcher.describeMismatch(actual.getNumberSold(), mismatchDescription.appendText("numberSold"));
				}

				return matches;
			}

			@Override
			public void describeTo(Description description) {
				description.appendText("Report matching ")
						   .appendValue(expected);
			}
		};
	}

	public static Report createReport(int itemId, String itemName, int numberSold) {
		Report report = new Report();
		report.setItemId(itemId);
		report.setItemName(itemName);
		report.setNumberSold(numberSold);

		return report;
	}

	public static void copyFileFromClasspath(String sourceFile, Path destinationPath) throws IOException {
		try (InputStream inputStream = CSVFileSystemReportDataAccessTest.class.getClassLoader().getResourceAsStream(sourceFile);
			 Writer writer = Files.newBufferedWriter(Files.createFile(destinationPath))) {
			IOUtils.copy(inputStream, writer);
		}
	}
}
