package org.asep.tdd.processor;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.asep.tdd.TestUtils;
import org.asep.tdd.dao.ReportProcessorDao;
import org.asep.tdd.data.Report;
import org.asep.tdd.repository.ReportDataAccess;
import org.asep.tdd.repository.exception.ReportReadingException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DumbReportProcessorTest {

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	private DumbReportProcessor reportProcessor;
	private ReportDataAccess reportDataAccess;
	private ReportProcessorDao reportProcessorDao;

	@Before
	public void setUp() throws Exception {
		reportDataAccess = mock(ReportDataAccess.class);
		reportProcessorDao = mock(ReportProcessorDao.class);

		reportProcessor = new DumbReportProcessor(reportDataAccess, reportProcessorDao);
	}

	@Test
	public void constructorThrowsIllegalArgumentExcetionWhenNullReportDataAccessPassed() throws Exception {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("reportDataAccess cannot be null");

		new DumbReportProcessor(null, reportProcessorDao);
	}

	@Test
	public void constructorThrowsIllegalArgumentExcetionWhenNullReportProcessorDaoPassed() throws Exception {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("reportProcessorDao cannot be null");

		new DumbReportProcessor(reportDataAccess, null);
	}

	@Test
	public void processReportsReportsAllItemsProcessed() throws Exception {
		List<Report> reports = Arrays.asList(TestUtils.createReport(132, "One", 8),
											 TestUtils.createReport(133, "Two", 2),
											 TestUtils.createReport(134, "Three", 4));

		when(reportDataAccess.getAllReports()).thenReturn(reports);

		reportProcessor.processReports();

		ArgumentCaptor<Set> itemIdsCaptor = ArgumentCaptor.forClass(Set.class);

		verify(reportProcessorDao).markReportItemsProcessed(itemIdsCaptor.capture());

		Set<Integer> itemIds = itemIdsCaptor.getValue();

		assertThat(itemIds, containsInAnyOrder(132, 133, 134));
	}

	@Test
	public void processReportsDoesNotMarkItemsProcessedWhenFailureToRetrieveReports() throws Exception {
		ReportReadingException exception = new ReportReadingException("TEST");

		when(reportDataAccess.getAllReports()).thenThrow(exception);

		reportProcessor.processReports();

		verify(reportProcessorDao, never()).markReportItemsProcessed(anySet());
	}
}