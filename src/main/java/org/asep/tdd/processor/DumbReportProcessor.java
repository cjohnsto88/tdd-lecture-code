package org.asep.tdd.processor;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.asep.tdd.dao.ReportProcessorDao;
import org.asep.tdd.data.Report;
import org.asep.tdd.repository.ReportDataAccess;
import org.asep.tdd.repository.exception.ReportReadingException;
import org.asep.tdd.utils.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DumbReportProcessor implements ReportProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(DumbReportProcessor.class);

	private final ReportDataAccess reportDataAccess;
	private final ReportProcessorDao reportProcessorDao;

	public DumbReportProcessor(ReportDataAccess reportDataAccess, ReportProcessorDao reportProcessorDao) {
		Assert.notNull(reportDataAccess, "reportDataAccess cannot be null");
		Assert.notNull(reportProcessorDao, "reportProcessorDao cannot be null");

		this.reportDataAccess = reportDataAccess;
		this.reportProcessorDao = reportProcessorDao;
	}

	@Override
	public void processReports() {

		LOGGER.info("Gathering reports");

		try {
			List<Report> allReports = reportDataAccess.getAllReports();

			LOGGER.info("Processing");

			Set<Integer> itemIds = allReports.stream()
											 .map(Report::getItemId)
											 .collect(Collectors.toSet());

			LOGGER.info("Items processed");

			reportProcessorDao.markReportItemsProcessed(itemIds);
		} catch (ReportReadingException e) {
			LOGGER.error("Failure when processing reports", e);
		}
	}
}
