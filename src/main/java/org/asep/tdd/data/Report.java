package org.asep.tdd.data;

public class Report {

	private int itemId;
	private String itemName;
	private int numberSold;

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getNumberSold() {
		return numberSold;
	}

	public void setNumberSold(int numberSold) {
		this.numberSold = numberSold;
	}

	@Override
	public String toString() {
		return "Report{" +
			   "itemId='" + itemId + '\'' +
			   ", itemName='" + itemName + '\'' +
			   ", numberSold=" + numberSold +
			   '}';
	}
}
