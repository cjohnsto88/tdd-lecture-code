package org.asep.tdd.utils;

public abstract class Assert {

	private Assert() {

	}

	public static void notNull(Object o, String message) {
		if (null == o) {
			throw new IllegalArgumentException(message);
		}
	}

	public static void isTrue(boolean b, String message) {
		if (!b) {
			throw new IllegalArgumentException(message);
		}
	}
}
