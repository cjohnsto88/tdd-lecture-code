package org.asep.tdd.repository.exception;

public class ReportReadingException extends Exception {

	public ReportReadingException(String message) {
		super(message);
	}

	public ReportReadingException(String message, Throwable cause) {
		super(message, cause);
	}
}
