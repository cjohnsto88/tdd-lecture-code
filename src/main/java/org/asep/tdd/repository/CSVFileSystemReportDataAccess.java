package org.asep.tdd.repository;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.asep.tdd.data.Report;
import org.asep.tdd.repository.exception.ReportReadingException;
import org.asep.tdd.utils.Assert;

public class CSVFileSystemReportDataAccess implements ReportDataAccess {

	private final Path baseDir;

	public CSVFileSystemReportDataAccess(Path baseDir) {
		Assert.notNull(baseDir, "baseDir cannot be null");
		Assert.isTrue(Files.isDirectory(baseDir), "baseDir must exist and be a directory");

		this.baseDir = baseDir;
	}

	@Override
	public List<Report> getAllReports() throws ReportReadingException {
		try {
			List<Report> reports = new ArrayList<>();

			Files.walkFileTree(baseDir, Collections.emptySet(), 1, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

					try (CSVParser csvRecords = CSVFormat.RFC4180.parse(Files.newBufferedReader(file))) {
						csvRecords.forEach(r -> {
							Report report = new Report();
							report.setItemId(Integer.parseInt(r.get(0)));
							report.setItemName(r.get(1));
							report.setNumberSold(Integer.parseInt(r.get(2)));

							reports.add(report);
						});
					}

					return FileVisitResult.CONTINUE;
				}
			});

			return reports;
		} catch (Exception e) {
			throw new ReportReadingException("Failure when reading reports in " + baseDir.toAbsolutePath(), e);
		}
	}
}
