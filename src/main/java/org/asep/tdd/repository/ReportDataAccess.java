package org.asep.tdd.repository;

import java.util.List;
import org.asep.tdd.data.Report;
import org.asep.tdd.repository.exception.ReportReadingException;

public interface ReportDataAccess {

	List<Report> getAllReports() throws ReportReadingException;

}
