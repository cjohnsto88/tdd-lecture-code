package org.asep.tdd.dao;

import java.util.Set;

public interface ReportProcessorDao {

	void markReportItemsProcessed(Set<Integer> itemIds);

}
